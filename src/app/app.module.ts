import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { RullproComponent } from './rullpro/rullpro.component';
import { RulletServices } from './sheried/rullet.services';
import { ColorDirective } from './sheried/color.directive';



@NgModule({
  declarations: [
    AppComponent,
    RullproComponent
  ],
  imports: [
    BrowserModule,
    ColorDirective
  ],
  providers: [RulletServices],
  bootstrap: [AppComponent]
})
export class AppModule { }
