import { Component } from '@angular/core';
import { RulletServices } from './sheried/rullet.services';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(public service: RulletServices) {
  }

}
